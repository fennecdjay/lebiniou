/*
 *  Copyright 1994-2021 Olivier Girondel
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */

function connect() {
    set_host_port();
    lbWs = new WebSocket(ws_url);


    lbWs.onopen = function() {
        // console.log("onopen");
    };


    lbWs.onmessage = function(event) {
        var payload = JSON.parse(event.data);
        if (!payload.ping) {
            // console.log("payload:", payload);
            if (payload.command && payload.result) {
                $('#lbtNbTests').css({ "color": "lime" });
                if (testing) {
                    random_command();
                    // setTimeout(random_command, 500);
                } else {
                    $('#lbtNbTests').css({ "color": "red" });
                }
            }
            if (payload.uiCommand && payload.result) {
                $('#lbtNbTests').css({ "color": "lime" });
                if (testing) {
                    random_command();
                    // setTimeout(random_command, 500);
                } else {
                    $('#lbtNbTests').css({ "color": "red" });
                }
            }
        }
    }


    lbWs.onclose = function(e) {
        // console.log("onclose");
    }


    lbWs.onerror = function(err) {
        // console.log("onerror");
        console.error('Socket encountered error: ', err.message, 'Closing socket');
        lbWs.close();
    }
}

var plugins;

function fuzzing() {
    return $('#lbtFuzzing').prop('checked');
}

function init_test() {
    $('#lbtSeed').val("Seed string");
    init_host_port(); 
    connect();
    fetch(`${httpUrl}/plugins`).then(result => {
        plugins = result;
        $('#lbtStatus').html(plugins.length + " plugins, " + NB_COMMANDS + " commands");
    });
    seed();
}


function random_integer(min, max) {
    return Math.floor(Math.random() * (max - min) + min);
}


function random_double(min, max) {
    return Math.random() * (max - min) + min;
}


function random_string() {
    switch (random_integer(0, 5)) {
    case 0:
        return "b4r";

    case 1:
        return "sequences";

    case 2:
        return "colormaps";

    case 3:
        return "images";

    case 4:
        return "webcams";
    }
}


function random_key() {
    switch (random_integer(0, 11)) {
    case 0:
        return "what";

    case 1:
        return "min";

    case 2:
        return "max";

    case 3:
        return "selectedParam";

    case 4:
        return "value";

    case 5:
        return "pos";

    case 6:
        return "f00";

    case 7:
        return "mode";

    case 8:
        return "x";

    case 9:
        return "y";

    case 10:
        return "z";
    }
}


function random_value() {
    switch (random_integer(0, 8)) {
    case 0: // integer
        return random_integer(-1000, 1000);

    case 1: // double
        return random_double(-1000, 1000);

    case 2: // boolean
        return random_integer(0, 2) ? true : false;

    case 3: // string
        return random_string();

    case 4: // plugin
        return random_plugin();

    case 5: // parameter
        return random_integer(-1, 10);

    case 6: // null
        return null;

    case 7: // array
        return [ random_value() ];
    }
}


function random_plugin() {
    return plugins[random_integer(0, plugins.length)];
}


function random_command() {
    nb_tests++;
    $('#lbtNbTests').html(nb_tests);
    test_command(random_integer(0, NB_COMMANDS));
    lbtChart.update();
}


function seed() {
    Math.seedrandom($('#lbtSeed').val());
}


var testing = 0;
var nb_tests = 0;


function start_test() {
    seed();
    testing = 1;
    nb_tests = 0;
    random_command();
}


function stop_test() {
    testing = 0;
    $('#lbtCommand').html('&nbsp;');
}


function random_boolean() {
    return random_integer(0, 2);
}


function random_plugins() {
    var a = [];
    var n = random_integer(0, 10);

    for (var i = 0; i < n; i++) {
        a.push(random_boolean() ? random_plugin() : random_value());
    }

    return a;
}


function change(x) {
    if (!fuzzing()) {
        return x;
    }

    switch (random_integer(0, 3)) {
    case 0:
        return x;

    case 1:
        return [ x ];

    case 2:
        return { "b4z": x };
    }
}


function testUiCommand(cmd) {
    switch (cmd) {
    case "UI_CMD_CONNECT":
        uiCommand(cmd);
        break;

    case "UI_CMD_APP_SELECT_PLUGIN":
    case "UI_CMD_APP_SET_FADE_DELAY":
    case "UI_CMD_APP_SET_MAX_FPS":
    case "UI_CMD_APP_SET_VOLUME_SCALE":
    case "UI_CMD_APP_TOGGLE_RANDOM_SCHEMES":
    case "UI_CMD_APP_TOGGLE_RANDOM_SEQUENCES":
    case "UI_CMD_COL_NEXT_N":
    case "UI_CMD_COL_PREVIOUS_N":
    case "UI_CMD_IMG_NEXT_N":
    case "UI_CMD_IMG_PREVIOUS_N":
        uiCommand(cmd, change(random_value()));
        break;

    case "UI_CMD_SEQ_REORDER":
        uiCommand(cmd, change(random_plugins()));
        break;

    case "UI_CMD_SEQ_SET_LAYER_MODE":
    case "UI_CMD_SEQ_SET_PARAM_CHECKBOX_VALUE":
    case "UI_CMD_SEQ_SET_PARAM_PLAYLIST_VALUE":
    case "UI_CMD_SEQ_SET_PARAM_SELECT_VALUE":
    case "UI_CMD_SEQ_SET_PARAM_SLIDER_VALUE":
    case "UI_CMD_APP_SET_3D_ROTATION_FACTOR":
    case "UI_CMD_TRACKBALL_ON_DRAG_START":
    case "UI_CMD_APP_SET_AUTO_MODE":
    case "UI_CMD_APP_SET_3D_ROTATION_AMOUNT":
        var k1 = random_key();
        var k2 = random_key();
        var v1 = random_value();
        var v2 = random_value();
        var arg = {};
        arg[k1] = v1;
        arg[k2] = v2;
        uiCommand(cmd, change(arg));
        break;

    case "UI_CMD_APP_SET_DELAY":
        var k1 = random_key();
        var k2 = random_key();
        var k3 = random_key();
        var v1 = random_value();
        var v2 = random_value();
        var v3 = random_value();
        var arg = {};
        arg[k1] = v1;
        arg[k2] = v2;
        arg[k3] = v3;
        uiCommand(cmd, change(arg));
        break;

    case "UI_CMD_NOOP":
    case "UI_CMD_TRACKBALL_ON_DRAG_MOVE":
    case "UI_CMD_TRACKBALL_ON_MOUSE_WHEEL":
        uiCommand('UI_CMD_NOOP', cmd);
        break;

    default:
        $('#lbtCommand').css({ "color": "red" });
        testing = 0;
    }
}


var lbtChart;

function create_chart() {
    var ctx = $('#lbtChartCanvas');
    lbtChart = new Chart(ctx, {
        type: 'horizontalBar',
        data: {
            labels: labels,
            datasets: [{
                label: '# of Calls',
                data: data,
                backgroundColor: 'lime',
		borderColor: 'green',
                borderWidth: 1
            }]
        },
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: true
                    }
                }]
            }
        }
    });
}
