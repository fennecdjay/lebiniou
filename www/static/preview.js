/*
 *  Copyright 1994-2021 Olivier Girondel
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */

"use strict";

let lbPreviewWs = null;
let lbPreviewUrl = null;
let lbPreviewFps = 1;
let lbPreviewDelay = -1;
let lbPreviewTimeout = null;
let lbPreviewFullscreen = false;
let lbImg = null;
let lbTotalFrames = 0;

const PREVIEW_WIDTH = 480;
const PREVIEW_HEIGHT = 270;


function previewConnect() {
    lbImg = document.getElementById('lbFrame');
    lbImg.onload = drawFrame;
    // lbImg.addEventListener('onload', drawFrame);

    if (!document.fullscreenEnabled) {
        $('#lbGoFullscreen').hide();
    }

    $('#lbFrameRefresh').html(`${lbPreviewFps} fps`);
    $('#lbFrameRefreshSlider').slider({
        min: 1,
        max: 25,
        values: [ lbPreviewFps ],
        slide: function(event, ui) {
            lbPreviewFps = ui.value;
            localStorage.setItem('lbPrefPreviewFps', lbPreviewFps);
            $('#lbFrameRefresh').html(`${lbPreviewFps} fps`);
            updateFrame();
        }
    });

    $('#lbFrame').attr('width', PREVIEW_WIDTH);
    $('#lbFrame').attr('height', PREVIEW_HEIGHT);

    console.log(`Preview connecting to ${lbPreviewUrl}`);
    lbPreviewWs = new WebSocket(lbPreviewUrl);


    lbPreviewWs.onopen = function() {
        console.info('Preview connected');
        if (lbPrefs.displayFrame) {
            updateFrame();
        }
    };


    lbPreviewWs.onmessage = function(event) {
        if (event.data) {
            // console.log(`Got frame ${++lbTotalFrames}`, event);
            setFrame(event.data);
        }
    }

    
    lbPreviewWs.onclose = function(e) {
        console.log('Preview websocket closed', e);
        lbPreviewWs = null;
        clearTimeout(lbPreviewTimeout);
        setTimeout(function() {
            previewConnect();
        }, 1000);
    }


    lbPreviewWs.onerror = function(err) {
        console.error('Preview websocket error', err);
        lbPreviewWs.close();
    }
}


function getFrame() {
    if (lbPrefs.displayFrame && lbPreviewWs) {
        // console.log(`Request ${lbPreviewFullscreen ? 'fullscreen' : 'rescaled'} frame`);
        if (lbPreviewFullscreen) {
            lbPreviewWs.send(JSON.stringify({ 'getFrame': null }));
        } else {
            lbPreviewWs.send(JSON.stringify({ 'getFrame': { width: PREVIEW_WIDTH, height: PREVIEW_HEIGHT } }));
        }
    }
}


function updateFrame() {
    if (lbPreviewTimeout) {
        clearTimeout(lbPreviewTimeout);
    }
    lbPreviewDelay = Math.round(1 / lbPreviewFps * 1000);
    // console.log(`updateFrame, lbPreviewFps: ${lbPreviewFps}, lbPreviewDelay: ${lbPreviewDelay}`);
    getFrame();
}


function setFrame(frame) {
    lbImg.src = URL.createObjectURL(frame);
}


function drawFrame() {
    URL.revokeObjectURL(this.src);
    // console.log('Frame rendered');
    clearTimeout(lbPreviewTimeout);
    lbPreviewTimeout = setTimeout(getFrame, lbPreviewDelay);
}


function previewFullscreen() {
    if (!lbPreviewFullscreen) {
        $('#lbFrame').attr('width', screenWidth);
        $('#lbFrame').attr('height', screenHeight);
    } else {
        $('#lbFrame').attr('width', PREVIEW_WIDTH);
        $('#lbFrame').attr('height', PREVIEW_HEIGHT);
    }
    document.getElementById('lbFrame').requestFullscreen();
    $('#lbFrame').on('fullscreenchange', (event) => {
        lbPreviewFullscreen = !lbPreviewFullscreen;
        // console.log('Fullscreen', lbPreviewFullscreen);
        if (!lbPreviewFullscreen) {
            $('#lbFrame').attr('width', PREVIEW_WIDTH);
            $('#lbFrame').attr('height', PREVIEW_HEIGHT);
        }
    });
}
