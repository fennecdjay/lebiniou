/*
 *  Copyright 1994-2021 Olivier Girondel
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */

"use strict";

const UI_HEIGHT = 180;


function colormapsCommandResult(r) {
    switch (r.command) {
    case "CMD_COL_PREVIOUS":
    case "CMD_COL_RANDOM":
    case "CMD_COL_NEXT":
        setColormap(r.result.colormap);
        return 1;
    }

    switch (r.uiCommand) {
    case "UI_CMD_CONNECT":
        setColormap(r.result.sequence.colormap);
        return 1;

    case "UI_CMD_COL_PREVIOUS_N":
    case "UI_CMD_COL_NEXT_N":
        setColormap(r.result.colormap);
        return 1;

    case "UI_CMD_SELECT_ITEM":
        if (r.result.item === "colormap") {
            setColormap(r.result.colormap);
        }
        return 1;
    }

    switch (r.vuiCommand) {
    case "VUI_SELECTOR_CHANGE":
        if (r.result.colormap) {
            setColormap(r.result.colormap);
            return 1;
        }
    }

    return 0;
}


function setColormap(name) {
    $('#lbColormap').html(name);
    fetch(`${lbHttpUrl}/colormap?name=${escape(name)}`).then(response => response.json()).then(json => drawColormap('lbColormapW3c', json.w3c, UI_HEIGHT));
}


function drawColormap(canvasId, colormap, height) {
    for (let c in colormap) {
        $(`#${canvasId}`).drawLine({
            strokeStyle: colormap[c],
            strokeWidth: 1,
            x1: c, y1: 0,
            x2: c, y2: height
        });
    }
}
