/*
 *  Copyright 1994-2021 Olivier Girondel
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */

"use strict";

const PICKER_HEIGHT = 40;
const ITEMS_PER_ROW  = 6;
let page = 0;


function setPage() {
    const p = getUrlParameter('page');
    if (p) {
        page = p - 1;
    }
    $(`#lbNavButton_${page}`).css({
        "color": "white",
        "background-color": "blue",
    });
}


function start() {
    $("#lbTitle").html(`Le Biniou - ${item} picker`);
    const a = (item === "image") ? "an" : "a";
    $('#lbHeader').html(`Click on ${a} ${item} to use it`);
    fetch('/data').then(response => response.json()).then(result => {
        const { colormaps, images, autoColormaps, autoImages } = result;
        let { randomModes } = result;
        if (autoColormaps) ++randomModes;
        if (autoImages) ++randomModes;

        const plural = (randomModes > 1) ? "s" : "";
        if (randomModes) {
            $('#lbWarning').html(`&#x26a0; You have ${randomModes} auto mode${plural} ON. This may change the ${item}. &#x26a0;&nbsp;<button onclick="stopAutoModes();" >Turn off</button><hr />`);
        }
        if (item === "colormap") {
            if (colormaps) {
                buildNavigation(colormaps);
                setPage();
                buildIndex(colormaps);
            }
        } else if (item === "image") {
            if (images) {
                buildNavigation(images);
                setPage();
                buildIndex(images);
            }
        }
    });
}


function gotoPage(page) {
    document.location = `/picker.html?item=${item}&page=${page+1}`;
}


function buildNavigation(items) {
    let items_rows = (item === "colormap") ? 11 : 6;
    const ITEMS_PER_PAGE = ITEMS_PER_ROW * items_rows;
    const nb_buttons = Math.floor(items / ITEMS_PER_PAGE) + 1;
    for (let i = 0; i < nb_buttons; ++i) {
        const html = `<div><button id="lbNavButton_${i}" class="lb-navigation" onclick="gotoPage(${i});">${i+1}</button></div>`;
        $('#lbNavigation').append(html);
    }
}


function getUrlParameter(sParam) {
    const sPageUrl = window.location.search.substring(1);
    const sUrlVariables = sPageUrl.split('&');
    for (let i in sUrlVariables) {
        const sParameterName = sUrlVariables[i].split('=');
        if (sParameterName[0] === sParam) {
            return sParameterName[1];
        }
    }
}


function buildIndex(items) {
    const items_rows = (item === "colormap") ? 11 : 6;
    const ITEMS_PER_PAGE = ITEMS_PER_ROW * items_rows;
    const start = page * ITEMS_PER_PAGE;
    const end = Math.min(start + ITEMS_PER_PAGE, items);

    for (let i = start; i < end; ++i) {
        const name = `lbItem_${i}`;
        let html;

        if (item === "colormap") {
            html = `<div><canvas id="${name}" class="lb-item" width="256" height="${PICKER_HEIGHT}" onclick="use(${i});"></canvas></div>`;
        } else if (item === "image") {
            html = `<div><img id="${name}" class="lb-item" width="192" height="108" onclick="use(${i});" /></div>`;
        }
        $('#lbItems').append(html);
    }

    for (let i = start; i < end; ++i) {
        switch (item) {
        case "colormap":
            fetch(`/colormap?idx=${i}`).then(response => {
                if (response.status === 200) {
                    response.json().then(function(data) {
                        const { name, w3c } = data;
                        const el = `lbItem_${i}`;
                        $(`#${el}`).attr("title", name);
                        drawColormap(el, w3c, PICKER_HEIGHT);
                    });
                }
            });
            break;

        case "image":
            fetch(`/image?idx=${i}&json=true`).then(response => {
                if (response.status === 200) {
                    response.json().then(function(data) {
                        const { name, path } = data;
                        const el = `lbItem_${i}`;
                        const short = name.split(".")[0];
                        $(`#${el}`).attr("title", short);
                        $(`#${el}`).attr("src", path);
                    });
                }
            });
            break;
        }
    }
}


function stopAutoModes() {
    fetch('/command/CMD_APP_STOP_AUTO_MODES', {
        method: 'post'
    }).then(() => $('#lbWarning').hide());
}


function use(index) {
    fetch('/ui/command/UI_CMD_SELECT_ITEM', {
        method: 'post',
        headers: {
            "Content-type": "application/json"
        },
        body: JSON.stringify({ "item": item, "index": index })
    });
}
