<!DOCTYPE html>
<html>
  <head>
    <title>Le Biniou</title>

    <meta charset="UTF-8">
    <meta name="mobile-web-app-capable" content="yes">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="stylesheet" type="text/css" href="/ui/static/lebiniou.mincss">
    <link rel="stylesheet" type="text/css" href="jquery-ui.min.css">
    <link rel="stylesheet" type="text/css" href="/ui/static/w3.mincss">

    <script type="text/javascript" src="jquery.min.js"></script>
    <script type="text/javascript" src="jquery-ui.min.js"></script>
    <script type="text/javascript" src="/ui/static/jquery.ui.touch-punch.min.js"></script>
    <script type="text/javascript" src="/ui/static/PhiloGL.js"></script>
    <script type="text/javascript" src="/ui/static/jcanvas.min.js"></script>

    <script type="text/javascript" src="/ui/static/lebiniou.minjs"></script>
    <script type="text/javascript" src="/ui/static/banks.minjs"></script>
    <script type="text/javascript" src="/ui/static/sequence.minjs"></script>
    <script type="text/javascript" src="/ui/static/colormaps.minjs"></script>
    <script type="text/javascript" src="/ui/static/images.minjs"></script>
    <script type="text/javascript" src="/ui/static/plugin.minjs"></script>
    <script type="text/javascript" src="/ui/static/plugins.minjs"></script>
    <script type="text/javascript" src="/ui/static/preview.minjs"></script>
    <script type="text/javascript" src="/ui/static/shortcuts.minjs"></script>
    <script type="text/javascript" src="/ui/static/trackball.minjs"></script>
    <script type="text/javascript" src="/ui/static/threed.minjs"></script>
  </head>
  <body>
    <div class="container">
      <div class="lb-header">
        <div class="lb-header-container">
          <div><img id="lbWebsocketStatus" src="/ui/static/icons8-wi-fi-disconnected-48.png"></div>
          <div class="lb-blink"><span id="lbConnecting">Connecting...</span></div>
          <div><input id="lbHost" type="text" onchange="setHostPort();"></div>
          <div><input id="lbPort" type="number" onchange="setHostPort();"></div>
          <div class="lb-tooltips" title="Show/hide tooltips"><input id="lbTooltips" type="checkbox" onchange="toggle('lbPrefDisplayTooltips');">&nbsp;Tooltips</div>
          <div class="lb-controls" title="Show/hide controls"><input id="lbControls" type="checkbox" onchange="toggle('lbPrefDisplayControls');">&nbsp;Controls</div>
          <div class="lb-wiki"><button title="Read the documentation on the wiki" onclick="window.open('https://gitlab.com/lebiniou/lebiniou/-/wikis/home');">Wiki</button></div>
          <div class="lb-support"><button title="Ask for questions on freenode, channel #lebiniou" onclick="window.open('https://webchat.freenode.net?#lebiniou');">Get help on #lebiniou</button></div>
          <div class="lb-paypal">
            <form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_blank">
              <input type="hidden" name="cmd" value="_s-xclick" />
              <input type="hidden" name="hosted_button_id" value="6022679" />
              <input type="image" name="submit" src="/ui/static/donate.png" alt="Donate with PayPal" />
            </form>
          </div>
          <div class="lb-copyright">Le Biniou <span id="lbVersion"></span> &copy;1994-2021 Olivier "<a target="_blank" href="https://www.patreon.com/oliv3" class="lb-patreon">oliv3</a>" Girondel&nbsp;<br />Powered by <a target="_blank" href="https://github.com/babelouest/ulfius">Ulfius</a> <span id="lbUlfiusVersion"></span> | Icons by <a target="_blank" href="https://icons8.com">Icons8</a>&nbsp;</div>
          <div><button id="lbPluginsButton" title="Plugins" onclick="window.open('/plugins.html');"><img src="/ui/static/icons8-plugin-48.png" /></button></div>
          <div><button id="lbSettingsButton" title="Settings" onclick="window.open('/settings.html');"><img src="/ui/static/icons8-settings-48.png" /></button></div>
          <div id="lbShutdown"><button id="lbShutdownButton" title="Quit" onclick="shutdown();"><img src="/ui/static/icons8-shutdown-48.png" /></div>
        </div>
      </div>
      <div id="lbBankSets" class="lb-bank-sets"></div>
      <div id="lbBanks" class="lb-banks"></div>

      <div class="lb-sidebar">
        <div class="lb-label">Uptime</div>
        <div id="lbUptime" class="lb-value"></div>

        <div class="lb-label">FPS</div>
        <div class="lb-value">
          <div id="lbMaxFps"></div>
          <div id="lbMaxFpsSlider"></div>
          <div id="lbFpsGauge" class="w3-deep-orange w3-large">
            <div id="lbFps" class="w3-container w3-deep-purple"></div>
          </div>
        </div>

        <div id="lbVolumeScaleLabel" class="lb-label">Volume</div>
        <div id="lbVolumeScaleContainer" class="lb-value">
          <div id="lbVolumeScale"></div>
          <div id="lbVolumeScaleSlider"></div>
        </div>

        <div id="lbMuteLabel" class="lb-label">Input</div>
        <div id="lbMuteContainer"><button class="lb-button lb-output-button" id="lbMuteButton" onclick="command('CMD_APP_FREEZE_INPUT');" title="Freeze input"><img id="lbMuteButtonImg" src="/ui/static/icons8-pause-button-30.png" /></button></div>

        <div class="lb-label">Fade</div>
        <div class="lb-value">
          <div id="lbFadeDelay"></div>
          <div id="lbFadeDelaySlider"></div>
        </div>

        <div class="lb-label">Delays</div>
        <div>
          <div id="lbAutoSequencesLabel">Sequences / Schemes</div>
          <div id="lbAutoSequencesRange" class="lb-value"></div>
          <div id="lbAutoSequencesSlider"></div>
          <div>
            Mode&nbsp;<select id="lbAutoSequencesMode" onchange="uiCommand('UI_CMD_APP_SET_AUTO_MODE', { what: 'sequences', mode: this.value });">
              <option value="cycle">Cycle</option>
              <option value="shuffle">Shuffle</option>
              <option value="random">Random</option>
            </select>
          </div>

          <div id="lbAutoColormapsLabel">Colormaps</div>
          <div id="lbAutoColormapsRange" class="lb-value"></div>
          <div id="lbAutoColormapsSlider"></div>
          <div>
            Mode&nbsp;<select id="lbAutoColormapsMode" onchange="uiCommand('UI_CMD_APP_SET_AUTO_MODE', { what: 'colormaps', mode: this.value });">
              <option value="cycle">Cycle</option>
              <option value="shuffle">Shuffle</option>
              <option value="random">Random</option>
            </select>
          </div>

          <div id="lbAutoImagesLabel">Images</div>
          <div id="lbAutoImagesRange" class="lb-value"></div>
          <div id="lbAutoImagesSlider"></div>
          <div>
            Mode&nbsp;<select id="lbAutoImagesMode" onchange="uiCommand('UI_CMD_APP_SET_AUTO_MODE', { what: 'images', mode: this.value });">
              <option value="cycle">Cycle</option>
              <option value="shuffle">Shuffle</option>
              <option value="random">Random</option>
            </select>
          </div>

          <div id="lbAutoWebcamsRow">
            <div id="lbAutoWebcalsLabel">Webcams</div>
            <div id="lbAutoWebcamsRange" class="lb-value"></div>
            <div id="lbAutoWebcamsSlider"></div>
            <div>
              Mode&nbsp;<select id="lbAutoWebcamsMode" onchange="uiCommand('UI_CMD_APP_SET_AUTO_MODE', { what: 'webcams', mode: this.value });">
                <option value="cycle">Cycle</option>
                <option value="shuffle">Shuffle</option>
                <option value="random">Random</option>
              </select>
            </div>
          </div>
        </div>

        <div class="lb-label"><button class="lb-button lb-label" onclick="toggle('lbPrefDisplayThreed');">3D</button></div>
        <div>
          <div id="lbThreed">
            <div id="lbTrackball"><canvas id="lbTrackballCanvas" style="border: none;" width="100" height="100" title="Rotate 3D scene"></canvas></div>
            <div class="lb-threed-parameters">
              <div id="lbBoundary" class="lb-label">Boundary</div>
              <div>
                <select id="lbBoundarySelect" class="lb-layer-mode" onchange="uiCommand('UI_CMD_SELECT_ITEM', { 'item': 'boundaryMode', 'mode': this.value });" title="Change 3D boundary shape">
                  <option value="none">None</option>
                  <option value="cube">Cube</option>
                  <option value="sphere_dots">Sphere 1</option>
                  <option value="sphere_wireframe">Sphere 2</option>
                </select>
              </div>

              <div class="lb-label lb-rotation">Rotations</div>
              <div><input type="checkbox" id="lbRotationsCheckbox" onclick="command('CMD_APP_TOGGLE_3D_ROTATIONS');" title="Toggle 3D rotations"></div>

              <div></div>
              <div><button id="lbRandomizeRotations" class="lb-button" onclick="command('CMD_APP_RANDOMIZE_3D_ROTATIONS');" title="Randomize rotations">Randomize</button></div>

              <div class="lb-sublabel">Amount</div>
              <div>
                <div id="lbRotationAmount" class="lb-value"></div>
                <div id="lbRotationAmountSlider"></div>
              </div>

              <div class="lb-sublabel">X</div>
              <div>
                <div id="lbRotationX" class="lb-value"></div>
                <div id="lbRotationFactorXSlider"></div>
              </div>

              <div class="lb-sublabel">Y</div>
              <div>
                <div id="lbRotationY" class="lb-value"></div>
                <div id="lbRotationFactorYSlider"></div>
              </div>

              <div class="lb-sublabel">Z</div>
              <div>
                <div id="lbRotationZ" class="lb-value"></div>
                <div id="lbRotationFactorZSlider"></div>
              </div>
            </div>
          </div>
        </div>

        <div id="lbOutputsLabel" class="lb-label"><button id="lbOutputsLabel" class="lb-button lb-label" onclick="toggle('lbPrefDisplayOutputs');">Output</button></div>
        <div id="lbOutputs">
          <div id="lbOutputSdl2" class="lb-sublabel lb-output">
            <button class="lb-button" onclick="command('CMD_APP_SWITCH_FULLSCREEN');"><img id="lbFullscreen" src="" /></button>
            <button class="lb-button" onclick="command('CMD_APP_SWITCH_CURSOR');" title="Show/hide mouse cursor"><img src="/ui/static/icons8-cursor-24.png" /></button>
          </div>
          <div id="lbOutputMp4" class="lb-sublabel lb-output">
            <button id="lbEncodingButton" class="lb-button lb-output-button" onclick=""><img id="lbEncoding" src="" /></button>
          </div>
        </div>
      </div>

      <div class="lb-content">
        <div class="lb-content-grid">
          <div class="lb-auto-modes">
            <button id="lbRandomSchemes" onclick="uiCommand('UI_CMD_APP_TOGGLE_RANDOM_SCHEMES');" title="Activate random schemes">Schemes</button>
            <button id="lbRandomSequences" onclick="uiCommand('UI_CMD_APP_TOGGLE_RANDOM_SEQUENCES');" title="Activate random sequences">Sequences</button>
            <button id="lbAutoColormaps" onclick="command('CMD_APP_TOGGLE_AUTO_COLORMAPS');" title="Activate random colormap changes">Colormaps</button>
            <button id="lbAutoImages" onclick="command('CMD_APP_TOGGLE_AUTO_IMAGES');" title="Activate random image change">Images</button>
            <button id="lbRandomStop" onclick="command('CMD_APP_STOP_AUTO_MODES');" class="lb-button lb-random-stop" title="Stop all random modes">&#x1f6d1;</button>
          </div>

          <div class="lb-colormap">
            <div><button class="lb-label lb-button" onclick="toggle('lbPrefDisplayColormap');" title="Click to show/hide the current colormap">Colormap</button></div>
            <div id="lbColormap" class="lb-value"></div>
            <div><button class="lb-button lb-picker" onclick="window.open('/picker.html?item=colormap');" title="Open the colormap picker">Colormap picker</button></div>
            <div id="lbColormapsNavigation">
              <button class="lb-button lb-navigation" onclick="uiCommand('UI_CMD_COL_PREVIOUS_N', 100);">&#x21da;</button>
              <button class="lb-button lb-navigation" onclick="uiCommand('UI_CMD_COL_PREVIOUS_N', 10);">&#x21d0;</button>
              <button class="lb-button lb-navigation" onclick="command('CMD_COL_PREVIOUS');">&#x2190;</button>
              <button class="lb-button lb-navigation" onclick="command('CMD_COL_RANDOM');">Random</button>
              <button class="lb-button lb-navigation" onclick="command('CMD_COL_NEXT');">&#x2192;</button>
              <button class="lb-button lb-navigation" onclick="uiCommand('UI_CMD_COL_NEXT_N', 10);">&#x21d2;</button>
              <button class="lb-button lb-navigation" onclick="uiCommand('UI_CMD_COL_NEXT_N', 100);">&#x21db;</button>
            </div>
            <div id="lbColormapsShortcuts"></div>
            <div id="lbColormapRow">
              <canvas id="lbColormapW3c" width="256" height="180"></canvas>
            </div>
          </div>

          <div class="lb-image">
            <div><button class="lb-label lb-button" onclick="toggle('lbPrefDisplayImage');" title="Click to show/hide the current image">Image</button></div>
            <div id="lbImage" class="lb-value"></div>
            <div><button class="lb-button lb-picker" onclick="window.open('/picker.html?item=image');" title="Open the image picker">Image picker</button></div>
            <div>
              <div id="lbImagesNavigation">
                <button class="lb-button lb-navigation" onclick="uiCommand('UI_CMD_IMG_PREVIOUS_N', 100);">&#x21da;</button>
                <button class="lb-button lb-navigation" onclick="uiCommand('UI_CMD_IMG_PREVIOUS_N', 10);">&#x21d0;</button>
                <button class="lb-button lb-navigation" onclick="command('CMD_IMG_PREVIOUS');">&#x2190;</button>
                <button class="lb-button lb-navigation" onclick="command('CMD_IMG_RANDOM');">Random</button>
                <button class="lb-button lb-navigation" onclick="command('CMD_IMG_NEXT');">&#x2192;</button>
                <button class="lb-button lb-navigation" onclick="uiCommand('UI_CMD_IMG_NEXT_N', 10);">&#x21d2;</button>
                <button class="lb-button lb-navigation" onclick="uiCommand('UI_CMD_IMG_NEXT_N', 100);">&#x21db;</button>
              </div>
            </div>
            <div id="lbImagesShortcuts"></div>
            <div id="lbImageRow">
              <img id="lbThumbnail" width="320" height="180" />
            </div>
          </div>

          <div class="lb-current">
            <div class="lb-frame">
              <div><button class="lb-label lb-button" onclick="toggle('lbPrefDisplayFrame');" title="Click to show/hide the preview">Preview</button></div>
              <div><input id="lbSequenceName" type="text" class="lb-value" onfocus="this.oldvalue = this.value;" onchange="if (this.value.length && confirm('Rename sequence to \'' + this.value + '\' ?')) { uiCommand('UI_CMD_SEQ_RENAME', this.value); } else { this.value = this.oldvalue; }" title="Edit the sequence name to rename it"></div>
              <div id="lbSequencesNavigation">
                <button onclick="command('CMD_APP_FIRST_SEQUENCE');" class="lb-button lb-button-unicode lb-navigation" title="First sequence"><img src="/ui/static/icons8-first-48.png" /></button>
                <button onclick="command('CMD_APP_PREVIOUS_SEQUENCE');" class="lb-button lb-button-unicode lb-navigation" title="Previous sequence"><img src="/ui/static/icons8-previous-48.png" /></button>
                <button onclick="command('CMD_APP_NEXT_SEQUENCE');" class="lb-button lb-button-unicode lb-navigation" title="Next sequence"><img src="/ui/static/icons8-next-48.png" /></button>
                <button onclick="command('CMD_APP_LAST_SEQUENCE');" class="lb-button lb-button-unicode lb-navigation" title="Last sequence"><img src="/ui/static/icons8-last-48.png" /></button>
              </div>
              <div>Random
                <div style="display: inline">
                  <button id="lbRandomScheme" class="lb-button lb-navigation" onclick="command('CMD_APP_RANDOM_SCHEME');" title="Create a random sequence from schemes">Scheme</button>
                </div>
                <div style="display: inline">
                  <button id="lbRandomSequence" class="lb-button lb-navigation" onclick="command('CMD_APP_RANDOM_SEQUENCE');" title="Use a saved sequence at random">Sequence</button>
                </div>
              </div>
              <div id="lbFrameContainer">
                <div><img id="lbFrame" /></div>
                <br >
                <div id="lbFrameRow">
                  <div id="lbFrameRefresh" style="text-align: center;"></div>
                  <div id="lbFrameRefreshSlider"></div>
                  <br />
                  <div id="lbGoFullscreen"><button onclick="previewFullscreen();" class="lb-button" title="Fullscreen"><img src="/ui/static/icons8-toggle-full-screen-24.png" /></button></div>
                </div>
              </div>
            </div>
          </div>

          <div class="lb-sequence">
            <div id="lbSequenceLabel" class="lb-label">Sequence</div>
            <div id="lbSequenceActions">
              <button onclick="saveSequence();" class="lb-button lb-navigation" title="Save this sequence as new">SAVE</button>
              <button onclick="updateSequence();" class="lb-button lb-navigation" title="Update this sequence">UPDATE</button>
              <button id="lbBypassWebcam" onclick="command('CMD_APP_SWITCH_BYPASS');" class="lb-button lb-button-unicode lb-navigation" title="Bypass webcams">&#x1f4f7;</button>
              <button onclick="clearFrame();" class="lb-button lb-navigation" title="Clear the current frame">CLEAR</button>
              <button onclick="resetSequence();" class="lb-button lb-navigation" title="Remove all plugins from this sequence">RESET</button>
            </div>

            <div class="lb-bandpass-container">
              <div class="lb-label lb-pandpass-label">Bandpass</div>
              <div id="lbBandpassRange" class="lb-value"></div>
              <div></div>
              <div id="lbBandpassSlider"></div>
            </div>

            <div>
              <ul id="lbSequence"></ul>
            </div>
          </div>

          <div class="lb-selected">
            <div class="lb-label" style="display: inline"><button class="lb-button lb-label" onclick="command('CMD_APP_TOGGLE_SELECTED_PLUGIN');" title="Add or remove the plugin">Plugin</button></div>
            <div id="lbLockButton" style="display: inline"><button id="lbLocked" class="lb-button lb-button-unicode lb-navigation" onclick="command('CMD_APP_LOCK_SELECTED_PLUGIN');" title="Lock the plugin">&#x1f513;</button></div>
            <div id="lbSelectedPluginDname" class="lb-value" style="display: inline"></div>

            <div id="lbParameters" class="lb-parameters"></div>
          </div>
        </div>
      </div>

      <div class="lb-plugins">
        <div id="lbPluginsLabel" class="lb-label">Plugins</div>
        <div>
          <div><button id="lbPluginsTab0" class="lb-plugins-tab" onclick="activateTab(0);">Sound</button></div>
          <div><button id="lbPluginsTab1" class="lb-plugins-tab" onclick="activateTab(1);">Graphics</button></div>
          <div><button id="lbPluginsTab2" class="lb-plugins-tab" onclick="activateTab(2);">Image</button></div>
          <div><button id="lbPluginsTab3" class="lb-plugins-tab" onclick="activateTab(3);">Blur</button></div>
          <div><button id="lbPluginsTab4" class="lb-plugins-tab" onclick="activateTab(4);">Displace</button></div>
          <div><button id="lbPluginsTab5" class="lb-plugins-tab" onclick="activateTab(5);">Warp</button></div>
          <div><button id="lbPluginsTab6" class="lb-plugins-tab" onclick="activateTab(6);">Lens</button></div>
          <div id="lbPluginsTabWebcam"><button id="lbPluginsTab7" class="lb-plugins-tab" onclick="activateTab(7);">Webcam</button></div>
          <div><button id="lbPluginsTab8" class="lb-plugins-tab" onclick="activateTab(8);">All</button></div>
          <div><button id="lbPluginsTab9" class="lb-plugins-tab" onclick="activateTab(9);">Favorites</button></div>
        </div>
        <div id="lbPlugins"></div>
      </div>
    </div>
    <script>$(document).ready(() => start());</script>
  </body>
</html>
