/*
 *  Copyright 1994-2021 Olivier Girondel
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __BINIOU_SCHEME_H
#define __BINIOU_SCHEME_H

#include <inttypes.h>
#include "options.h"


typedef struct SchemeItem_s {
  double             p;            // probability to add the plugin (default: 1.0)
  uint8_t            mandatory;    // 1 if proba is 1.0
  uint8_t            disable_lens; // do not set this plugin as lens if added
  enum PluginOptions options;      // or-ed list of options that a plugin must have to be added
} SchemeItem_t;


enum AutoMode { AM_RANDOM = 0, AM_ENABLE, AM_DISABLE };

typedef struct Scheme_s {
  SchemeItem_t **items;
  uint8_t size;
  enum AutoMode auto_colormaps;
  enum AutoMode auto_images;
} Scheme_t;


Scheme_t *Scheme_new(const uint8_t);
void Scheme_delete(Scheme_t *);

enum AutoMode Scheme_str2AutoMode(const char *);
const char *Scheme_AutoMode2str(const enum AutoMode);

#endif /* __BINIOU_SCHEME_H */
