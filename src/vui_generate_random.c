/*
 *  Copyright 1994-2021 Olivier Girondel
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */

#include "vui.h"
#include "globals.h"


json_t *
vui_generate_random(Context_t *ctx, const json_t *arg)
{
  json_t *res = NULL;

  if (NULL != schemes) {
    const uint8_t disable_auto_modes = json_boolean_value(json_object_get(arg, "disableAutoModes"));
    Schemes_random(ctx, disable_auto_modes);
    Alarm_init(ctx->a_random);
    if (disable_auto_modes) {
      ctx->random_mode = BR_NONE;
    }
    res = json_pack("{so sb sb}",
                    "sequence", Sequence_to_json(ctx, ctx->sm->cur, 1, 0, UNSAVED_SEQUENCE),
                    "randomSchemes", (ctx->random_mode == BR_SCHEMES) || (ctx->random_mode == BR_BOTH),
                    "randomSequences", (ctx->random_mode == BR_SEQUENCES) || (ctx->random_mode == BR_BOTH));
  }

  return res;
}
