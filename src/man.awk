BEGIN {

}


function pmod(mod) {
    if (mod == "-")
	return;
    if (mod == "A")
	return "Alt-";
    if (mod == "C")
	return "Ctrl-";
    if (mod == "CA")
	return "Ctrl-Alt-";
    if (mod == "S")
	return "Shift-";
    if (mod == "CS")
	return "Ctrl-Shift-";
    if (mod == "AS")
	return "Alt-Shift-";
    if (mod == "CAS")
	return "Ctrl-Alt-Shift-";
}


{
    if (($1 == "#") || ($0 == "") || ($1 == "*") || ($1 == "-") || ($1 == "**") || ($3 == "-"))
	next;
  
    tail = substr($0, (length($1 $2 $3 $4) + 5));

    if (tail != "") {
	printf "[B<%s%s>] - %s\n", pmod($2), $3, tail;
	print "";
    } else {
	printf "[%s%s] - *** TODO document me ! ***\n", pmod($2), $3;
	print "";
    }
}


END {

}
