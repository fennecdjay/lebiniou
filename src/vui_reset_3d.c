/*
 *  Copyright 1994-2021 Olivier Girondel
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */

#include "vui.h"


json_t *
vui_reset_3d(Context_t *ctx)
{
  json_t *res = NULL;

#ifdef DEBUG_COMMANDS
  printf(">>> VUI_RESET_3D\n");
#endif
  Params3d_set_defaults(&ctx->params3d);
  res = Params3d_to_json(&ctx->params3d);
  
  return res;
}
