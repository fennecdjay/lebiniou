/*
 *  Copyright 1994-2021 Olivier Girondel
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */

#include "bulfius.h"
#include "context.h"
#include "defaults.h"


extern uint8_t max_fps;
extern uint16_t vui_port;
extern uint8_t launch_browser;
uint8_t vui_started = 0;


#define VUI_PAGES 7
static const char *vui_pages[VUI_PAGES] = { "/", "/colormaps", "/images", "/settings", "/plugins", "/remote", "/documentation" };


void
Context_start_vui(Context_t *ctx)
{
  // Initialize instance
  if (ulfius_init_instance(&ctx->vui_instance, vui_port, NULL, NULL) != U_OK) {
    xerror("ulfius_init_instance error on port %d, aborting\n", vui_port);
  }

  // Endpoint list declaration
  // GET
  for (uint8_t i = 0; i < VUI_PAGES; i++) {
    ulfius_add_endpoint_by_val(&ctx->vui_instance, BULFIUS_GET, vui_pages[i], NULL, 0, &callback_vui_get_static, (void *)BULFIUS_VUI_INDEX);
  }
  ulfius_add_endpoint_by_val(&ctx->vui_instance, BULFIUS_GET, BULFIUS_VUI_FAVICO_URL, NULL, 0, &callback_vui_get_static, (void *)BULFIUS_VUI_FAVICO);
  ulfius_add_endpoint_by_val(&ctx->vui_instance, BULFIUS_GET, BULFIUS_VUI_CSS_ROOT, BULFIUS_VUI_STATIC_FMT, 0, &callback_vui_get_static, (void *)BULFIUS_VUI_CSS);
  ulfius_add_endpoint_by_val(&ctx->vui_instance, BULFIUS_GET, BULFIUS_VUI_IMG_ROOT, BULFIUS_VUI_STATIC_FMT, 0, &callback_vui_get_static, (void *)BULFIUS_VUI_IMG);
  ulfius_add_endpoint_by_val(&ctx->vui_instance, BULFIUS_GET, BULFIUS_VUI_JS_ROOT, BULFIUS_VUI_STATIC_FMT, 0, &callback_vui_get_static, (void *)BULFIUS_VUI_JS);
  ulfius_add_endpoint_by_val(&ctx->vui_instance, BULFIUS_GET, BULFIUS_VUI_MEDIA_ROOT, BULFIUS_VUI_STATIC_FMT, 0, &callback_vui_get_static, (void *)BULFIUS_VUI_MEDIA);
  ulfius_add_endpoint_by_val(&ctx->vui_instance, BULFIUS_GET, BULFIUS_VUI_DEMOS_ROOT, BULFIUS_VUI_STATIC_FMT, 0, &callback_vui_get_static, (void *)BULFIUS_VUI_DEMOS);

  // Start the framework
  if (ulfius_start_framework(&ctx->vui_instance) == U_OK) {
    VERBOSE(printf("[i] Started ulfius framework on port %d\n", ctx->vui_instance.port));
  } else {
    xerror("Error starting ulfius framework on port %d\n", ctx->vui_instance.port);
  }

  vui_started = 1;

  if (launch_browser) {
    gchar *cmd = g_strdup_printf("/usr/bin/xdg-open http://localhost:%d", vui_port);
    int ret = system(cmd);
    if (ret == -1) {
      xperror("system");
    }
    g_free(cmd);
  }
}


void
Context_stop_vui(Context_t *ctx)
{
  for (uint8_t i = 0; i < VUI_PAGES; i++) {
    ulfius_remove_endpoint_by_val(&ctx->vui_instance, BULFIUS_GET, vui_pages[i], NULL);
  }
  ulfius_remove_endpoint_by_val(&ctx->vui_instance, BULFIUS_GET, BULFIUS_VUI_FAVICO_URL, NULL);
  ulfius_remove_endpoint_by_val(&ctx->vui_instance, BULFIUS_GET, BULFIUS_VUI_CSS_ROOT, NULL);
  ulfius_remove_endpoint_by_val(&ctx->vui_instance, BULFIUS_GET, BULFIUS_VUI_IMG_ROOT, NULL);
  ulfius_remove_endpoint_by_val(&ctx->vui_instance, BULFIUS_GET, BULFIUS_VUI_JS_ROOT, NULL);
  ulfius_remove_endpoint_by_val(&ctx->vui_instance, BULFIUS_GET, BULFIUS_VUI_MEDIA_ROOT, NULL);
  ulfius_remove_endpoint_by_val(&ctx->vui_instance, BULFIUS_GET, BULFIUS_VUI_DEMOS_ROOT, NULL);

  ulfius_stop_framework(&ctx->vui_instance);
  ulfius_clean_instance(&ctx->vui_instance);
}
