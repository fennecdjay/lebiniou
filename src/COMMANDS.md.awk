BEGIN {
    print "## Available commands";

    SHORTCUT_WIDTH=20
    DESCRIPTION_WIDTH=50
    COMMAND_WIDTH = 40
}


function pmod(mod) {
    if (mod == "-")
	return "";
    if (mod == "A")
	return "Alt-";
    if (mod == "C")
	return "Ctrl-";
    if (mod == "S")
	return "Shift-";
    if (mod == "CA")
	return "Ctrl-Alt-";
    if (mod == "CS")
	return "Ctrl-Shift-";
    if (mod == "AS")
	return "Alt-Shift-";
    if (mod == "CAS")
	return "Ctrl-Alt-Shift-";
}


function mkline(len, prefix, suffix) {
    printf prefix;
    for (i = 0; i < len; i++) {
        printf "-";
    }
    printf suffix;
}


function pad(len) {
    for (i = 0; i < len; i++) {
        printf " ";
    }
}


{
    if (($1 == "#") || ($0 == "") || ($1 == "-") || ($1 == "**") || ($3 == "-"))
	next;

    if ($1 == "*") {
	printf "\n###";
        for (i = 2; i <= NF; i++) {
            printf " " $i;
        }
        print "\n";

        SHORTCUT = "Shortcut";
        DESCRIPTION = "Description";
        COMMAND = "Command";
        
        printf "| ";
        pad(SHORTCUT_WIDTH - length(SHORTCUT));
        printf "%s |", SHORTCUT;
        pad(DESCRIPTION_WIDTH - length(DESCRIPTION));
        printf " %s |", DESCRIPTION;
        pad(COMMAND_WIDTH - length(COMMAND));
        printf "%s |\n", COMMAND;
        
        printf "%s", mkline(SHORTCUT_WIDTH, "|-", "-|");
        printf "%s", mkline(DESCRIPTION_WIDTH, "-", "-|");
        printf "%s\n", mkline(COMMAND_WIDTH, "-", "|");
    } else {
	tail = substr($0, (length($1 $2 $3 $4) + 5));

        printf "| ";
        pad(SHORTCUT_WIDTH - (length(pmod($2)) + length($3)));
        printf "%s%s | ", pmod($2), $3;
        pad(DESCRIPTION_WIDTH - length(tail));
        printf "%s |", tail;
        pad(COMMAND_WIDTH - length($4));
        printf "%s |\n", $4;
    }
}


END {

}
