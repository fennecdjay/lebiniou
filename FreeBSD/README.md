# FreeBSD Port

From the [Porter's Handbook](https://www.freebsd.org/doc/en/books/porters-handbook/quick-porting.html):

```sh
make
make makesum
make stage
make check-orphans
make package
make install
make deinstall
make package
```
